require 'socket'
require 'sinatra'
require 'rqrcode'

set :bind, '0.0.0.0'

get '/inventory' do
  @assets = [
    10001,
    10002,
    10003,
    10004
  ]

  erb :inventory, :layout => :layout
end

get '/inventory/:asset/image' do
  this_hostname = (ENV["COMPUTERNAME"] || `hostname`).strip!
  this_port = request.port
  asset = params[:asset]
  target = "http://#{this_hostname}:#{this_port}/inventory/#{asset}/audit"
  qrcode = RQRCode::QRCode.new(target)

  content_type 'image/png'
  return qrcode.as_png(
    fill: 'white',
    color: 'black',
    size: 128,
    border_modules: 4,
    module_px_size: 2
  ).to_s
end

get '/inventory/:asset/audit' do
  
end